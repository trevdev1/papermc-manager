"""
Main entrypoint for the papermc_manager program.
"""
import os

import click
from click.exceptions import Abort

from papermc_manager.updater.commands import update


@click.group()
@click.option(
    "--directory",
    "-d",
    help="Directory containing the server files, for use"
    " when not running this program from the server directory.",
)
def entry_point(directory=None):
    """
    Provides utilities for managing papermc servers.
    """
    if directory is not None:
        try:
            os.chdir(directory)
        except OSError as error:
            click.echo(error)
            raise Abort() from error


entry_point.add_command(update)

if __name__ == "__main__":
    entry_point()
