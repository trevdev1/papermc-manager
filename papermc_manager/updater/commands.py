"""
Manages and updates papermc servers.
"""

import os

import click
from packaging import version
import requests

JAR_PREFIX = "paper-"
JAR_SUFFIX = ".jar"
API_URL = "https://papermc.io/api/v2"


def get_latest_major_version_number():
    """
    Gets the latest major version of minecraft paper
    """
    url = f"{API_URL}/projects/paper"
    resp = requests.get(url, timeout=(10, 30))
    resp.raise_for_status()
    return max(version.parse(v) for v in resp.json()["versions"])


def get_latest_build_number(major_version):
    """
    Gets the latest build of `version` of papermc
    """
    url = f"{API_URL}/projects/paper/versions/{major_version}"
    resp = requests.get(url, timeout=(10, 30))
    resp.raise_for_status()
    return max(resp.json()["builds"])


def get_jar(major_version, build):
    """
    Gets the jar of specified version and build.
    """
    filename = f"paper-{major_version}-{build}.jar"
    url = f"{API_URL}/projects/paper/versions/{major_version}/builds/{build}/downloads/{filename}"

    with requests.get(url, timeout=(10, 120), stream=True) as request_stream:
        request_stream.raise_for_status()
        with open(filename, "wb") as jarfile:
            for chunk in request_stream.iter_content(chunk_size=8192):
                jarfile.write(chunk)
    return os.path.abspath(filename)


def clean_local_dir():
    """
    Gets the local version(s) of papermc in folder `path`.
    This returns a map of 'Version' objects to the absolute path to the file
    """
    local_jars = {
        version.parse(entry.name[6:-4]): entry.name
        for entry in os.scandir()
        if entry.name.startswith("paper-") and entry.name.endswith(".jar")
    }
    return local_jars


@click.command()
@click.option(
    "--keep", "-k", is_flag=True, help="Whether to keep all jars (no file deletion)."
)
@click.option(
    "--force",
    "-f",
    is_flag=True,
    help="Whether to force downloading the latest jar even if it is already installed.",
)
@click.option("--verbose", "-v", is_flag=True, help="Enables verbose logging.")
def update(keep: bool, force: bool, verbose):
    """
    Downloads the latest papermc server, and removes all older ones.
    """
    major_version = get_latest_major_version_number()
    build = get_latest_build_number(major_version)

    local_file = f"paper-{major_version}-{build}.jar"

    # get the jar if we need it or if we are forcing
    if force or not os.path.isfile(local_file):
        if verbose:
            click.echo(f"Downloading paper {major_version} build {build}.")
        local_file = get_jar(major_version, build)
        if verbose:
            click.echo(f"File saved as: {local_file}")
    else:
        click.echo("Latest jar is already installed. Exiting...")
        return

    local_jars = clean_local_dir()
    local_jars.pop(max(local_jars))

    if verbose:
        click.echo(
            f"Found local jars: {[os.path.basename(jar) for jar in local_jars.values()]}"
        )

    # delete all other jars
    if not keep:
        if verbose:
            click.echo("deleting...")
        for jar in local_jars.values():
            os.remove(jar)
