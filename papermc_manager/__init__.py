"""
The papermc management utility provides various
functions for managing a local papermc server.

These include updating a local server.
"""

from .paper import entry_point
