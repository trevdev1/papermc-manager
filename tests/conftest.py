"""
Common fixtures for testing.
"""

from unittest.mock import MagicMock

import pytest
from requests import Response


@pytest.fixture(name="m_response")
def create_mock_response():
    """
    Mock response object for use with requests functions.
    """
    return MagicMock(spec=Response)
