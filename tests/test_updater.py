"""
Tests for the updater module.
"""


from unittest.mock import patch


from papermc_manager.updater import commands


def test_get_latest_version(m_response):
    """
    Test getting the latest dictionary
    """
    m_response.json.return_value = {"versions": ["1.1", "1.2", "1.3", "1.3.1"]}
    with patch("requests.get", return_value=m_response):
        assert str(commands.get_latest_major_version_number()) == "1.3.1"
